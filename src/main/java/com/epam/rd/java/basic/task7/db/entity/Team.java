package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
//		return null;
		Team team=new Team();
		team.setId(0);
		team.setName(name);
		return team;
	}

	public String toString(){
		return name;
	}

	public boolean equals(Object o){
		Team team=(Team) o;
		return this.name.equals(team.name);
	}

}
