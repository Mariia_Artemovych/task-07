package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.google.protobuf.CodedOutputStream;


public class DBManager {

	String selectUsers="select * from users";
	String selectUser="select * from users where login=?";
	String selectTeam="select * from teams where name=?";
	String deleteTeam="delete from teams where name=?";
	String deleteUser="delete from users where login=?";
	String selectTeamById="select name from teams where id=?";
	String selectTeams="select * from teams";
	String insertUser="insert into users (login) values (?)";
	String insertTeam="insert into teams (name) values (?)";
	String setTeamForUser="insert into users_teams (user_id,team_id) values (?,?)";
//	String getUserTeams="select team_id from users_teams where user_id=?";
	String updateTeam="update teams set name=? where id=?";
	String getUserTeams="SELECT teams.id,teams.name\n" +
			"FROM\n" +
			"users_teams\n" +
			"INNER JOIN users ON users_teams.user_id=users.id\n" +
			"INNER JOIN teams ON users_teams.team_id=teams.id\n" +
			"WHERE USERS.login=?";
//	Connection connection;

//	private static final String JDBC_URL="jdbc:mysql://localhost:3306/test2db";
//	private static final String USER="root";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
//		return null;
		if (instance==null){
			instance=new DBManager();
		}
		return instance;
	}

	private DBManager()  {
		
	}
	
	public Connection connect(){
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Properties properties=new Properties();
			try (InputStream inputStream= Files.newInputStream(Paths.get("app.properties"))){
				properties.load(inputStream);
			} catch (IOException exception) {
				exception.printStackTrace();
			}
			String JDBC_URL=properties.getProperty("connection.url");
//			String USER=properties.getProperty("user");
//			String PASSWORD=properties.getProperty("password");
		try {
			connection=DriverManager.getConnection(JDBC_URL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public List<User> findAllUsers() throws DBException {
//		return null;
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<User>users=new ArrayList<>();
		User user;
		Connection connection=connect();
		try {
			statement =connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			resultSet=statement.executeQuery(selectUsers);
			while (resultSet.next()){
				user= new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
				users.add(user);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
//		return false;
		Connection connection=connect();
//		boolean flag = false;
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(insertUser,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1,user.getLogin());
			if (preparedStatement.executeUpdate()>0){
				try(ResultSet resultSet=preparedStatement.getGeneratedKeys()){
					if (resultSet.next()){
						user.setId(resultSet.getInt(1));
					}
				}
				return true;
			}
//			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
//		return false;
		Connection connection=connect();

			try {
				PreparedStatement preparedStatement=connection.prepareStatement(deleteUser);
				for (User user:users) {
				preparedStatement.setString(1,user.getLogin());
				preparedStatement.execute();
			}
			} catch (SQLException e) {
				e.printStackTrace();
			}finally {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return true;

	}

	public User getUser(String login) throws DBException {
//		return null;
		Connection connection=connect();
		ResultSet resultSet= null;
		User user=new User();
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(selectUser);
			preparedStatement.setString(1,login);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()){
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return user;
	}

	public Team getTeam(String name) throws DBException {
//		return null;
		Connection connection=connect();
		ResultSet resultSet= null;
		Team team=new Team();
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(selectTeam);
			preparedStatement.setString(1,name);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
//		return null;
		List<Team>teams=new ArrayList<>();
		Connection connection=connect();
		ResultSet resultSet= null;
		try {
			resultSet = connection.createStatement().executeQuery(selectTeams);
			while (resultSet.next()){
				Team team= new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
//		return false;
//		boolean flag=false;
		Connection connection=connect();
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(insertTeam,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1,team.getName());
			if (preparedStatement.executeUpdate()>0){
				ResultSet resultSet=preparedStatement.getGeneratedKeys();
				if (resultSet.next()){
					team.setId(resultSet.getInt(1));
					return true;
				}
			}
//			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
//		return false;
		Connection connection=connect();
		try {
			connection.setAutoCommit(false);
			PreparedStatement statement=connection.prepareStatement(setTeamForUser);
			for (Team team:teams) {
				statement.setInt(1,user.getId());
				statement.setInt(2,team.getId());
//				statement.addBatch();
				statement.executeUpdate();
			}

//			statement.executeBatch();

			connection.commit();
		} catch (SQLException e) {
//			e.printStackTrace();
			try {
				connection.rollback();
				throw new DBException("err",e);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
//		return null;
		Connection connection=connect();
		List<Team>teams=new ArrayList<>();

		try {
			PreparedStatement preparedStatement=connection.prepareStatement(getUserTeams);
			preparedStatement.setString(1,user.getLogin());
			ResultSet resultSet=preparedStatement.executeQuery();

			while (resultSet.next()){
				Team team=new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				teams.add(team);
			}
//			preparedStatement.close();
//			PreparedStatement preparedStatement1=connection.prepareStatement(selectTeamById);
//			for (Team team:teams) {
//				preparedStatement1.setInt(1,team.getId());
//				ResultSet resultSet1=preparedStatement1.executeQuery();
//				if (resultSet1.next()){
//					team.setName(resultSet1.getString("name"));
//				}
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
//		return false;
//		boolean flag=false;
		Connection connection=connect();
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(deleteTeam);
			preparedStatement.setString(1,team.getName());
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
//		return false;
		Connection connection= connect();

		try {
			PreparedStatement preparedStatement=connection.prepareStatement(updateTeam);
			preparedStatement.setString(1,team.getName());
			preparedStatement.setInt(2,team.getId());
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}
