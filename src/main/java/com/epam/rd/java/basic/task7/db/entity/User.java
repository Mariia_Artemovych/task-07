package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
//		return null;
		User user=new User();
		user.setId(0);
		user.setLogin(login);
		return user;
	}


	public boolean equals(Object o){
		User user=(User)o;
		return this.login.equals(user.login);
	}

	@Override
	public String toString() {
		return login;
	}
}
